(($, Drupal, drupalSettings) => {
  'use strict';

  /**
   * Overrides the core openDialog method add sends a postmessage request to the
   * parent window instead of making an Ajax call directly.
   */
  Drupal.ckeditor5.openDialog = function(url, saveCallback, dialogSettings) {
    // Add a consistent dialog class.
    const classes = dialogSettings.dialogClass
      ? dialogSettings.dialogClass.split(' ')
      : [];
    classes.push('ui-dialog--narrow');
    dialogSettings.dialogClass = classes.join(' ');
    dialogSettings.autoResize =
      window.matchMedia('(min-width: 600px)').matches;
    dialogSettings.width = 'auto';

    url += (url.indexOf('?') < 0 ? '?' : '&') + `me_id=${drupalSettings.mercuryEditorId}`;

    const settings = {
      dialog: dialogSettings,
      dialogType: 'modal',
      selector: '.ckeditor5-dialog-loading-link',
      url,
      progress: { type: 'fullscreen' },
      submit: {
        editor_object: {},
      },
    };
    window.parent.postMessage({
      type: 'drupalAjax',
      settings
    });

    // Store the save callback to be executed when this dialog is closed.
    Drupal.ckeditor5.saveCallback = saveCallback;
  }

  /**
   * @namespace Drupal.mercuryEditor.editors
   */
  Drupal.mercuryEditor.editors.ckeditor5 = (() => {

    /**
     * Generates a unique ID with an optional prefix.
     *
     * @param {string} prefix
     *   The optional prefix string to prepend to the generated ID.
     *
     * @returns {string}
     *   The generated unique ID.
     */
    const generateUniqueID = (prefix) => {
      return `${prefix}-${Math.random().toString(36).substr(2, 9)}`;
    };

    /**
     * Creates an inline toolbar element.
     *
     * @returns {Element}
     *   The created inline toolbar element.
     */
    const createInlineToolbar = () => {
      const toolBar = document.createElement('div');

      toolBar.id = generateUniqueID('toolbar');
      toolBar.classList.add('me-inline-editor__toolbar');

      return toolBar;
    };

    /**
     * Fetches the untransformed text for a field.
     *
     * @param {Element} element
     *   The element containing the field.
     *
     * @returns {Promise}
     *   A promise that resolves with the untransformed text.
     *
     * @throws {Error}
     *   If an error occurs during the fetching process.
     */
    const fetchFieldUntransformedText = (element) => {
      return new Promise((resolve, reject) => {
        try {
          const field = Drupal.mercuryEditor.inlineEditor.getFieldElement(element);
          const endpoint = field.getAttribute('data-me-untransformed-endpoint');

          if (endpoint) {
            const untransformedTextAjax = Drupal.ajax({
              url: endpoint,
            });

            untransformedTextAjax.commands.editorGetUntransformedText =
              (ajax, response) => {
                resolve(response.data);
              };

            untransformedTextAjax.execute();
          }
        } catch (error) {
          reject(error);
        }
      });
    };

    /**
     * Fetches the rendered HTML for a field.
     *
     * @param {Element} element
     *   The element containing the field.
     *
     * @returns {Promise}
     *   A promise that resolves with the rendered markup.
     *
     * @throws {Error}
     *   If an error occurs during the fetching process.
     */
    const fetchRenderedField = (element) => {
      return new Promise((resolve, reject) => {
        try {
          const field = Drupal.mercuryEditor.inlineEditor.getFieldElement(element);
          const endpoint = field.getAttribute('data-me-rendered-field-endpoint');

          if (endpoint) {
            const renderedFieldAjax = Drupal.ajax({
              url: endpoint,
            });

            renderedFieldAjax.commands.editorGetRenderedField =
              (ajax, response) => {
                resolve(response.data);
              };

            renderedFieldAjax.execute();
          }
        } catch (error) {
          reject(error);
        }
      });
    };

    /**
     * Creates inline content for the given element.
     *
     * @param {HTMLElement} element
     *   The element to create inline content for.
     *
     * @returns {HTMLElement}
     *   The created inline content element.
     */
    const createInlineContent = async (element) => {
      const content = document.createElement('div');
      content.classList.add('me-inline-editor__content');

      const text = await fetchFieldUntransformedText(element);
      content.innerHTML = text;

      return content;
    };

    /**
     * Creates rendered field content for the given element.
     *
     * @param {HTMLElement} element
     *   The element to create inline content for.
     *
     * @returns {HTMLElement}
     *   The created inline content element.
     */
    const createRenderedContent = async (element) => {
      const renderedContent = await fetchRenderedField(element);
      return renderedContent;
    }

    /**
     *
     * @param event
     */
    const onClickOutsideElement = (event) => {
      if (!event.target.closest('[data-me-edit-mode="true"], .ck, [role="dialog"]')) {
        const element = document.querySelector('[data-me-edit-mode="true"]');
        if (element) {
          Drupal.mercuryEditor.inlineEditor.detachEditor(element);
        }
      }
    };

    /**
     * Initializes CKEditor for a given element with the specified ID.
     *
     * @param {string} id
     *   The ID of the CKEditor instance.
     * @param {HTMLElement} element
     *   The element to associate with the editor instance.
     *
     * @returns {Promise<void>}
     *   A promise that resolves once CKEditor is initialized.
     */
    const initializeCKEditor = async (id, element) => {
      try {
        const editor = await waitForCKEditorInstance(id);
        editor.editing.view.focus();
        editor.model.document.on('change:data', Drupal.debounce(() => {
          const content = editor.getData();

          if (content) {
            const field = Drupal.mercuryEditor.inlineEditor.getFieldElement(element);
            const formatId = field.getAttribute('data-me-format_id') || undefined;

            Drupal.mercuryEditor.inlineEditor.saveEditorContent(
              element,
              content,
              { format_id: formatId }
            );

            document.dispatchEvent(
              new CustomEvent('mercuryEditor.ckEditor5:change:data', {
                uuid: editor.uuid,
                data: {
                  content: content
                }
              }),
            );
          }
        }, 300));
        const isNew = element.closest('.me-inline-editor-isnew');

        if (isNew) {
          isNew.classList.remove('me-inline-editor-isnew');
          editor.editing.view.focus();
        }
      } catch (error) {
        console.error('An error occurred while initializing CKEditor:', error);
      }
    };

    /**
     * Position the toolbar within the viewport.
     *
     * @param {HTMLElement} toolbar
     */
    const positionToolbar = (toolbar) => {
      const toolbarRect = toolbar.getBoundingClientRect();
      const toolbarRight = toolbarRect.right;
      const viewportWidth = window.innerWidth;

      if (toolbarRight > viewportWidth) {
        const offset = toolbarRight - viewportWidth;
        toolbar.style.left = `-${offset}px`;
      }
    }

    /**
     * Waits for the CKEditor instance with the given ID to be available.
     *
     * @param {string} id
     *   The ID of the CKEditor instance to wait for.
     *
     * @returns {Promise}
     *   A promise that resolves to the CKEditor instance when it becomes
     *   available.
     */
    const waitForCKEditorInstance = (id) => {
      return new Promise((resolve) => {
        const interval = setInterval(() => {
          const editorInstance = Drupal.CKEditor5Instances.get(id);
          if (editorInstance) {
            clearInterval(interval);
            resolve(editorInstance);
          }
        }, 100);
      });
    };

    /**
     * Retrieves the Drupal editor based on the given editor ID.
     *
     * @param {string} editor_id
     *   The ID of the editor to retrieve.
     *
     * @returns {object|undefined}
     *   The Drupal editor object if found or undefined if not.
     */
    const getDrupalEditor = (editor_id) => {
      return Drupal.editors[editor_id] || undefined;
    };

    /**
     * Retrieves the editor format based on the given format ID.
     *
     * @param {string} format_id
     *   The ID of the format to retrieve.
     * @returns {Object|undefined}
     *   The editor format object, or undefined if not found.
     */
    const getDrupalEditorFormat = (format_id) => {
      return typeof drupalSettings !== 'undefined'
        ? drupalSettings?.editor?.formats[format_id]
        : undefined;
    };

    /**
     * Retrieves the format ID associated with the editor for the given element.
     *
     * @param {Element} element
     *   The element to retrieve the format ID for.
     * @returns {string|undefined}
     *   The format ID associated with the editor, or undefined if not found.
     */
    const getEditorFormatId = (element) => {
      return Drupal.mercuryEditor.inlineEditor.getFieldElement(element)
        .getAttribute('data-me-format_id') || undefined;
    }

    /**
     * Retrieves the data of the CKEditor5 editor instance associated with
     * the given element.
     *
     * @param {Element} element
     *   The element attached with the CKEditor5 editor instance.
     *
     * @returns {string|undefined}
     *   The data from the editor instance, otherwise undefined.
     */
    const getEditorInstanceData = (element) => {
      const id = element.getAttribute('data-ckeditor5-id');

      if (id != undefined) {
        const editorInstance = Drupal.CKEditor5Instances.get(id);
        return editorInstance.getData();
      }

      return undefined;
    }

    /**
     * Retrieves the information about the editor for a given element.
     *
     * @param {Element} element
     *   The element to retrieve the editor information for.
     *
     * @returns {Object}
     *   An object containing the format and editor information.
     */
    const getEditorInfo = (element) => {
      const format = getDrupalEditorFormat(getEditorFormatId(element));

      return {
        format: format,
        editor: getDrupalEditor(format.editor),
      };
    };

    return {
      attach: async (element) => {

        element.textContent = '';

        const {editor, format} = getEditorInfo(element);

        const toolBar = createInlineToolbar();
        const content = await createInlineContent(element);

        element.prepend(content);
        element.prepend(toolBar);

        element.classList.add('me-inline-editor__wrapper');

        const placeholder = element.getAttribute('data-field-placeholder');

        if (placeholder) {
          format.editorSettings.config.placeholder = placeholder;
        }

        editor.attachInlineEditor(content, format, toolBar.id);

        const id = content.getAttribute('data-ckeditor5-id');

        initializeCKEditor(id, element)
          .then(() => {
            positionToolbar(toolBar);
            document.addEventListener('mouseup', onClickOutsideElement);
          })
          .catch((error) => {
            console.error(
              'An error occurred while initializing CKEditor:',
              error,
            );
          });
      },
      detach: async (element) => {
        document.removeEventListener('mouseup', onClickOutsideElement);

        const {editor, format} = getEditorInfo(element);
        const toolBar = element.querySelector('.me-inline-editor__toolbar');
        const content = element.querySelector('.me-inline-editor__content');
        const renderedField = await createRenderedContent(element);
        editor.detach(content);

        if (renderedField) {
          content.parentElement.innerHTML = renderedField;
        }

        toolBar.remove();
        content.remove();

        element.classList.remove('me-inline-editor__wrapper');
      },
    };
  })();

})(jQuery, Drupal, drupalSettings);
