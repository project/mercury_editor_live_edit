(($, Drupal, drupalSettings) => {

  /**
   * @namespace Drupal.mercuryEditor.editors
   */
  Drupal.mercuryEditor.editors.basic = (() => {

    /**
     * Make field content editable.
     *
     * @param {Element} element
     *   The target element where the editable content will be injected.
     * @param {string} text
     *   The initial text content of the editable field.
     */
    const makeContentEditable = (element, text) => {
      const field = Drupal.mercuryEditor.inlineEditor.getFieldElement(element)
      const editableDiv = document.createElement('div');

      editableDiv.classList.add('editable-content');
      editableDiv.contentEditable = 'true';
      editableDiv.textContent = text;

      if (!text) {
        const placeholder = document.createElement('div');
        placeholder.classList.add('editable-placeholder');
        placeholder.textContent = 'Click to edit';
        editableDiv.appendChild(placeholder);
      }
      field.innerHTML = '';
      field.appendChild(editableDiv);

      editableDiv.focus();

      editableDiv.addEventListener('blur', () => {
        Drupal.mercuryEditor.inlineEditor.detachEditor(element);
      });

      editableDiv.addEventListener('input', Drupal.debounce(() => {
        const content = editableDiv.textContent;

        Drupal.mercuryEditor.inlineEditor.saveEditorContent(
          element,
          content
        );
      }, 300));
    };

    return {
      attach: (element) => {
        const field = Drupal.mercuryEditor.inlineEditor
          .getFieldElement(element)

        field.addEventListener('click', () => {
          if (!field.hasAttribute('data-me-field-is-editable')) {
            const text = field.textContent;
            field.setAttribute('data-me-field-is-editable', 'true');

            makeContentEditable(element, text);
          }
        });
      },
      detach: (element) => {
        const field = Drupal.mercuryEditor.inlineEditor
          .getFieldElement(element)

        if (field.hasAttribute('data-me-field-is-editable')) {
          const editableDiv = field.querySelector('.editable-content');

          const text = editableDiv.textContent;
          field.append(text);
          field.removeChild(editableDiv);
          field.removeAttribute('data-me-field-is-editable');
        }
      },
    };
  })();
})(jQuery, Drupal, drupalSettings);
