((Drupal, window) => {
  'use strict';

  /**
   * Listen for post messages on the preview window.
   */
  window.parent.addEventListener('message', async (event) => {
    if (event.data.type == 'meUpdatePreviewField') {
      const settings = event.data.settings || {};

      if (
        settings?.sync_id
        && settings?.content != undefined
      ) {
        const syncId = settings?.sync_id;
        const content = settings?.content;

        const inputField = document.querySelector(
          `[data-me-editor_field_preview_sync_id="${syncId}"]`
        );
        const isEditMode = inputField
          .getAttribute('data-me-edit-mode') ?? 'false';

        if (inputField && isEditMode === 'false') {
          inputField.innerHTML = content;
        }
      }
    }
  });
})(Drupal, window);
