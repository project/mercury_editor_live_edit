((Drupal, window, once) => {

  /**
   * Waits for the CKEditor instance with the given ID to be available.
   *
   * @param {string} id
   *   The ID of the CKEditor instance to wait for.
   *
   * @returns {Promise}
   *   A promise that resolves to the CKEditor instance when it becomes
   *   available.
   */
  const waitForCKEditorInstance = (id) => {
    return new Promise((resolve) => {
      const interval = setInterval(() => {
        const editorInstance = Drupal.CKEditor5Instances.get(id);
        if (editorInstance) {
          clearInterval(interval);
          resolve(editorInstance);
        }
      }, 100);
    });
  };

  /**
   * Listen for post messages on the parent window.
   */
  window.addEventListener('message', async (event) => {
    const data = event.data || {};
    const settings = data.settings || {};

    if (
      data.type == 'meUpdateTempstore'
      && settings.url != undefined
    ) {
      Drupal.ajax({
        url: settings.url,
        submit: settings?.params ?? {},
      }).execute();
    }

    if (data.type == 'meUpdateParentField') {
      if (
        settings?.sync_id
        && settings?.content != undefined
      ) {
        const syncId = settings?.sync_id;
        const content = settings?.content;

        const inputField = document.querySelector(
          `[data-me-editor_field_parent_sync_id="${syncId}"] .form-element`,
        );

        if (inputField) {
          const id = inputField.getAttribute('data-ckeditor5-id');

          if (id) {
            const editor = await waitForCKEditorInstance(id);
            editor.setData(content);
          } else {
            inputField.value = content;
          }
        }
      }
    }
  });

  /**
   * Waits for a specific data attribute to be added or modified on an element
   * and executes a callback function.
   *
   * @param {Element} element
   *   The element to observe for attribute changes.
   * @param {string} attributeName
   *   The name of the data attribute to observe.
   * @param {Function} callback
   *   The callback function to execute when the attribute changes.
   *
   * @returns {Function}
   *   A function to disconnect the observer.
   */
  const waitForDataAttribute = (element, attributeName, callback) => {
    const observer = new MutationObserver((mutations) => {
      for (const mutation of mutations) {
        if (mutation.type === 'attributes' && mutation.attributeName === attributeName) {
          if (element.hasAttribute(attributeName)) {
            observer.disconnect();
            callback(element);
          }
        }
      }
    });

    observer.observe(element, {
      attributes: true,
      attributeFilter: [attributeName]
    });

    return function disconnectObserver() {
      observer.disconnect();
    };
  }

  /**
   * Callback function used to initialize CKEditor5 instance on an element.
   *
   * @param {HTMLElement} element
   *   The element on which the CKEditor5 instance is being initialized.
   */
  const ckeditor5InstanceCallback = async (element)=> {
    const id = element.getAttribute('data-ckeditor5-id');
    const editor = await waitForCKEditorInstance(id);

    const field = element.closest('[data-me-editor_field_parent_sync_id]');
    const syncId = field.getAttribute('data-me-editor_field_parent_sync_id');

    editor.model.document.on('change:data', Drupal.debounce(() => {
      const content = editor.getData();

      const field = element
        .closest('[data-me-editor_field_parent_sync_id]');
      const formatId = field.getAttribute('data-me-format_id') || undefined;

      Drupal.mercuryEditor.inlineEditor.saveEditorContent(
        element,
        content,
        { format_id: formatId }
      );

      window.postMessage({
        type: 'meUpdatePreviewField',
        settings: {
          sync_id: syncId,
          content: content,
        },
      });
    }));
  }

  /**
   * Defines the MercuryEditorInlineEditorSync behavior.
   *
   * This behavior is responsible for synchronizing the data between the
   * parent and the preview field.
   */
  Drupal.behaviors.mercuryEditorInlineEditorSync = {
    attach: () => {
      once('me-inline-editor-parent-sync', '[data-me-editor_field_parent_sync_id] .form-element')
        .forEach(async (element) => {
          const field = element.closest('[data-me-editor_field_parent_sync_id]');
          const editor = field.getAttribute('data-me-editor_id');

          if (editor === 'ckeditor5') {
            if (element.hasAttribute('data-ckeditor5-id')) {
              ckeditor5InstanceCallback(element);
            } else {
              waitForDataAttribute(element, 'data-ckeditor5-id', ckeditor5InstanceCallback);
            }
          }
          else {
            element.addEventListener('input', (element) => {
              const content = element.target.value;
              window.postMessage({
                type: 'meUpdatePreviewField',
                settings: {
                  sync_id: syncId,
                  content: content
                },
              });
            })
          }
        });
    }
  };
})(Drupal, window, once);
