(($, Drupal, drupalSettings, once) => {
  'use strict';

  Drupal.registerLpbMoveError((settings, el, target) => {
    if (target.closest('[data-me-live-edit-enabled]')) {
      return "Can't move an element when in live edit mode.";
    }
  }, 'moves');

  /**
   * Drupal.mercuryEditor
   *
   * -----------------------
   * Module for integrating the Mercury Editor with Drupal.
   * -----------------------
   *
   * @module Drupal.mercuryEditor
   */
  Drupal.mercuryEditor = Drupal.mercuryEditor || {};

  /**
   * @namespace Drupal.mercuryEditor.editors
   *
   * @description
   *   This namespace contains the editors for the Mercury Editor module in Drupal.
   */
  Drupal.mercuryEditor.editors = Drupal.mercuryEditor.editors || {};

  /**
   * Creates an inline editor for the Mercury editor in Drupal.
   *
   * @class
   * @memberOf Drupal.mercuryEditor
   */
  Drupal.mercuryEditor.inlineEditor = (() => {

    /**
     * Retrieves the editor ID from the given element.
     *
     * @param {HTMLElement} element
     *   The element to retrieve the editor ID from.
     *
     * @returns {string}
     *   The editor ID.
     */
    const getEditorId = (element) =>  {
      const field = module.getFieldElement(element);
      return field.getAttribute('data-me-editor_id');
    }

    /**
     * Retrieves the inline editor for the given element.
     *
     * @param {Element} element
     *   The HTML element for which the inline editor is to be retrieved.
     * @returns {Object|undefined}
     *   The inline editor object, or undefined if not found.
     */
    const loadInlineEditorByElement = (element) => {
      const editorId = getEditorId(element);
      return Drupal.mercuryEditor.editors[editorId] || undefined;
    }

    const module = {

      /**
       * Attach the inline editor from the specified element.
       *
       * @param {HTMLElement} element
       *   The element to attach the editor to.
       */
      attachEditor: (element) => {
        if (element.classList.contains('me-editor-attached')) {
          return;
        }

        // Check for an exisitng editor and detach it.
        const existingInstanceElement = document.querySelector('[data-me-edit-mode="true"]');
        if (existingInstanceElement) {
          Drupal.mercuryEditor.inlineEditor.detachEditor(existingInstanceElement);
        }

        element.classList.add('me-editor-attached');

        const meElement = element.closest('[data-me-edit-screen-key]');
        if (meElement) {
          meElement.setAttribute('data-me-live-edit-enabled', 'true');
        }

        const editor = loadInlineEditorByElement(element);

        if (editor !== undefined) {
          editor.attach(element);

          const field = module.getFieldElement(element);

          field.setAttribute('data-me-edit-mode', 'true');
        }
      },

      /**
       * Detach the inline editor from the specified element.
       *
       * @param {HTMLElement} element
       *   The element to detach the editor from.
       */
      detachEditor: (element) => {
        if (!element.classList.contains('me-editor-attached')) {
          return;
        }
        element.classList.remove('me-editor-attached');

        const meElement = element.closest('[data-me-edit-screen-key]');
        if (meElement) {
          meElement.removeAttribute('data-me-live-edit-enabled');
        }

        const editor = loadInlineEditorByElement(element);

        if (editor !== undefined) {
          editor.detach(element);

          const field = module.getFieldElement(element);

          field.setAttribute('data-me-edit-mode', 'false');
        }
      },

      /**
       * Saves the editor field content for a given entity type.
       *
       * @param {HTMLElement} element
       *   The inline editor element.
       * @param {string} content
       *   The inline editor content.
       */
      saveEditorContent: (element, content, submit_values = {}) => {
        try {
          const field = module.getFieldElement(element);

          const fieldName = field
            .getAttribute('data-me-field_name') || undefined;
          const entityUuid = field
            .getAttribute('data-me-entity_uuid') || undefined;

          const endpoint = field.getAttribute('data-me-endpoint');
          const editorPluginId = field.getAttribute('data-me-editor_plugin_id');

          if (!endpoint || !entityUuid || !fieldName) {
            throw new Error(
              'Unable to save the inline editor content due to invalidate ' +
              'parameters.'
            )
          }

          window.parent.postMessage({
            type: 'meUpdateTempstore',
            settings: {
              url: endpoint,
              params: {
                uuid: entityUuid,
                content: content,
                field_name: fieldName,
                editor_plugin_id: editorPluginId,
                ...submit_values,
              },
            }
          });

          window.parent.postMessage({
            type: 'meUpdateParentField',
            settings: {
              sync_id: field.getAttribute('data-me-editor_field_preview_sync_id'),
              content: content,
            }
          })
        } catch (error) {
          console.error(error.message)
        }
      },

      /**
       * Get the inline editor field element.
       *
       * @param {Element} element
       *   The element to start searching from.
       *
       * @returns {Element|null}
       *   The closest ancestor element with the [data-me-field_name] attribute,
       *   or null if not found.
       */
      getFieldElement: (element) => {
        return element.closest('[data-me-field_name]');
      },
    };

    return module;
  })();

  Drupal.behaviors.mercuryEditorInlineEditor = {
    attach: () => {
      once('me-inline-editor', '.me-is-editable')
        .forEach((element) => {
          element.addEventListener('mousedown', () => {
            Drupal.mercuryEditor.inlineEditor.attachEditor(element);
          });
      });
    }
  }
})(jQuery, Drupal, drupalSettings, once);
