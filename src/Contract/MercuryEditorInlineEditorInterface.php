<?php

declare(strict_types=1);

namespace Drupal\mercury_editor_live_edit\Contract;

use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Interface for mercury_editor_inline_editors plugins.
 */
interface MercuryEditorInlineEditorInterface {

  /**
   * The inline editor plugin label.
   */
  public function label(): string;

  /**
   * The inline editor to use.
   *
   * @return string
   */
  public function getEditor(): string;

  /**
   * The inline editor plugin field types.
   *
   * @return array
   *   An array of the field types.
   */
  public function getFieldTypes(): array;

  /**
   * The inline editor plugin attachments.
   *
   * @param array $element
   *  The field element array.
   *
   * @return array
   *   An array of Drupal attachments.
   */
  public function getAttachments(array $element): array;

  /**
   * Attach attributes to the inline editor field.
   *
   * @param array $element
   *   The field element array.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity the element is associated with.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition the element is associated with.
   *
   * @return array
   *   An array of the attributes to attach the element.
   */
  public function attachAttributes(
    array $element,
    ContentEntityInterface $entity,
    FieldDefinitionInterface $field_definition
  ): array;

  /**
   * Save the inline editor plugin content.
   *
   * @param string $content
   *   The content that's been updated.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request for this change.
   * @param string $field_name
   *    The field name on the entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *    The entity instance.
   *
   * @return void
   */
  public function saveContent(
    string $content,
    Request $request,
    string $field_name,
    ContentEntityInterface $entity,
  ): void;

}
