<?php

declare(strict_types=1);

namespace Drupal\mercury_editor_live_edit\Plugin\MercuryEditorInlineEditor;

use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\mercury_editor_live_edit\Annotation\MercuryEditorInlineEditor;

/**
 * Define the mercury editor plain text inline editor plugin.
 *
 * @MercuryEditorInlineEditor(
 *   id = "plain_text",
 *   editor = "basic",
 *   label = @Translation("Plain Text Inline Editor"),
 *   description = @Translation("Allow plain text fields to be updated inline."),
 *   field_types = { "string", "string_long" }
 * )
 */
class MercuryEditorInlineEditorPlainText extends MercuryEditorInlineEditorPluginBase {

  /**
   * {@inheritDoc}
   */
  public function saveContent(
    string $content,
    Request $request,
    string $field_name,
    ContentEntityInterface $entity
  ): void {
    $entity->{$field_name}->setValue([
      'value' => $content,
    ]);
  }

}
