<?php

declare(strict_types=1);

namespace Drupal\mercury_editor_live_edit\Plugin\MercuryEditorInlineEditor;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\mercury_editor_live_edit\Contract\MercuryEditorInlineEditorInterface;

/**
 * Define the mercury editor inline editor plugin base.
 */
abstract class MercuryEditorInlineEditorPluginBase extends PluginBase implements MercuryEditorInlineEditorInterface {

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritDoc}
   */
  public function getEditor(): string {
    return $this->pluginDefinition['editor'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldTypes(): array {
    return $this->pluginDefinition['field_types'] ?: [];
  }

  /**
   * {@inheritdoc}
   */
  public function attachAttributes(
    array $element,
    ContentEntityInterface $entity,
    FieldDefinitionInterface $field_definition
  ): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachments(array $element): array {
    $attachments['library'][] = "mercury_editor_live_edit/inline_editor.{$this->getEditor()}";

    return $attachments;
  }

}
