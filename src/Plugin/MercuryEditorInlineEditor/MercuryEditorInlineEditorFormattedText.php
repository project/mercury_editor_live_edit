<?php

declare(strict_types=1);

namespace Drupal\mercury_editor_live_edit\Plugin\MercuryEditorInlineEditor;

use Drupal\node\NodeInterface;
use Drupal\editor\Plugin\EditorManager;
use Drupal\Component\Utility\NestedArray;
use Drupal\paragraphs\ParagraphInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\layout_paragraphs\LayoutParagraphsLayout;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mercury_editor_live_edit\Annotation\MercuryEditorInlineEditor;

/**
 * Define the mercury editor formatted text inline editor plugin.
 *
 * @MercuryEditorInlineEditor(
 *   id = "formatted_text",
 *   editor = "ckeditor5",
 *   label = @Translation("Formatted Text Inline Editor"),
 *   description = @Translation("Allow formatted text to be updated inline."),
 *   field_types = { "text_long" }
 * )
 */
class MercuryEditorInlineEditorFormattedText extends MercuryEditorInlineEditorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The editor manager.
   *
   * @var \Drupal\editor\Plugin\EditorManager
   */
  protected EditorManager $editorManager;

  /**
   * The plugin class constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\editor\Plugin\EditorManager $editor_manager
   *   The editor manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EditorManager $editor_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->editorManager = $editor_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.editor')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function saveContent(
    string $content,
    Request $request,
    string $field_name,
    ContentEntityInterface $entity,
  ): void {
    if ($format_id = $request->request->get('format_id')) {
      $entity->{$field_name}->setValue([
        'value' => $content,
        'format' => $format_id
      ]);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getAttachments(array $element): array {
    $format_id = $this->getElementFormat($element);

    return NestedArray::mergeDeep(
      parent::getAttachments($element),
      $this->editorManager->getAttachments([$format_id] ?? [])
    );
  }

  /**
   * {@inheritDoc}
   */
  public function attachAttributes(
    array $element,
    ContentEntityInterface $entity,
    FieldDefinitionInterface $field_definition
  ): array {
    return [
      'data-me-format_id' => $this->getElementFormat($element),
      'data-field-placeholder' => new TranslatableMarkup('Input text...'),
      'data-me-untransformed-endpoint' => $this->resolveUntransformedTextEndpoint(
        $entity,
        $field_definition->getName()
      ),
      'data-me-rendered-field-endpoint' => $this->resolveRenderedFieldEndpoint(
        $entity,
        $field_definition->getName()
      ),
    ];
  }

  /**
   * Get the format ID from the element.
   *
   * @param array $element
   *   The array containing the elements.
   *
   * @return string
   *   The format value of the first array element.
   */
  protected function getElementFormat(array $element): string {
    return $element[0]['#format'] ?? 'basic_html';
  }

  /**
   * Resolve the Mercury editor untransformed endpoint.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to resolve the endpoint for.
   *
   * @return string|null
   *   The Mercury editor endpoint, otherwise null if the entity type is
   *   unsupported.
   */
  protected function resolveUntransformedTextEndpoint(
    ContentEntityInterface $entity,
    string $field_name
  ): ?string {

    if ($entity instanceof ParagraphInterface) {
      /** @var LayoutParagraphsLayout $layout_paragraph_layout */
      $layout_paragraph_layout = $entity->_layoutParagraphsLayout;
      $layout_id = $layout_paragraph_layout->id();

      return implode(DIRECTORY_SEPARATOR, [
        '/mercury-editor-inline-editor',
        'layout',
        $layout_id,
        $field_name,
        $entity->uuid(),
        'untransformed',
      ]);
    }

    if ($entity instanceof NodeInterface) {
      return implode(DIRECTORY_SEPARATOR, [
        '/mercury-editor-inline-editor',
        'host',
        $entity->uuid(),
        $field_name,
        'untransformed',
      ]);
    }

    return NULL;
  }

  /**
   * Resolve the Mercury editor rendered field endpoint.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to resolve the endpoint for.
   * @param string $field_name
   *   The field name.
   *
   * @return string|null
   *   The Mercury editor endpoint, otherwise null if the entity type is
   *   unsupported.
   */
  protected function resolveRenderedFieldEndpoint(
    ContentEntityInterface $entity,
    string $field_name
  ): ?string {

    if ($entity instanceof ParagraphInterface) {
      /** @var \Drupal\layout_paragraphs\LayoutParagraphsLayout $layout_paragraph_layout */
      $layout_paragraph_layout = $entity->_layoutParagraphsLayout;
      $layout_id = $layout_paragraph_layout->id();

      return implode(DIRECTORY_SEPARATOR, [
        '/mercury-editor-inline-editor',
        'layout',
        $layout_id,
        $field_name,
        $entity->uuid(),
        'rendered',
      ]);
    }

    if ($entity instanceof NodeInterface) {
      return implode(DIRECTORY_SEPARATOR, [
        '/mercury-editor-inline-editor',
        'host',
        $entity->uuid(),
        $field_name,
        'untransformed',
      ]);
    }

    return NULL;
  }

}
