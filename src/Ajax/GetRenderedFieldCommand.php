<?php

declare(strict_types=1);

namespace Drupal\mercury_editor_live_edit\Ajax;

use Drupal\Core\Ajax\BaseCommand;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;

/**
 * AJAX command to rerender formatted text field without transformation filters.
 */
class GetRenderedFieldCommand extends BaseCommand {

  use CommandWithAttachedAssetsTrait;

  /**
   * Constructs a GetRenderedFieldCommand object.
   *
   * @param string|array $content
   *   The data to pass on to the client side.
   */
  public function __construct(protected $content) {
    parent::__construct('editorGetRenderedField', $this->getRenderedContent());
  }

}
