<?php

declare(strict_types=1);

namespace Drupal\mercury_editor_live_edit\Controller;

use Drupal\Core\Utility\Error;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\filter\Plugin\FilterInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\ContentEntityInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\mercury_editor\MercuryEditorTempstore;
use Drupal\layout_paragraphs\LayoutParagraphsLayout;
use Drupal\mercury_editor\MercuryEditorContextService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mercury_editor_live_edit\Ajax\GetRenderedFieldCommand;
use Drupal\mercury_editor_live_edit\Ajax\GetUntransformedTextCommand;
use Drupal\layout_paragraphs\LayoutParagraphsLayoutTempstoreRepository;
use Drupal\mercury_editor_live_edit\MercuryEditorInlineEditorPluginManager;
use Drupal\mercury_editor_live_edit\Contract\MercuryEditorInlineEditorInterface;

/**
 * Define the Mercury editor inline editor controller.
 */
class MercuryEditorInlineEditorController extends ControllerBase {

  /**
   * The class constructor.
   *
   * @param \Drupal\mercury_editor\MercuryEditorContextService $mercuryEditorContextService
   *   The mercury editor context service.
   * @param \Drupal\mercury_editor\MercuryEditorTempstore $mercuryEditorTempstore
   *   The Mercury Editor tempstore service.
   * @param \Drupal\layout_paragraphs\LayoutParagraphsLayoutTempstoreRepository $layoutParagraphsLayoutTempstore
   *   The layout paragraph layout tempstore service.
   * @param \Drupal\mercury_editor_live_edit\MercuryEditorInlineEditorPluginManager $mercuryEditorInlineEditorPluginManager
   *   The mercury editor inline editor plugin manager.
   */
  public function __construct(
    protected MercuryEditorContextService $mercuryEditorContextService,
    protected MercuryEditorTempstore $mercuryEditorTempstore,
    protected LayoutParagraphsLayoutTempstoreRepository $layoutParagraphsLayoutTempstore,
    protected MercuryEditorInlineEditorPluginManager $mercuryEditorInlineEditorPluginManager,
  ) {
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('mercury_editor.context'),
      $container->get('mercury_editor.tempstore_repository'),
      $container->get('layout_paragraphs.tempstore_repository'),
      $container->get('plugin.manager.mercury_editor_inline_editor'),
    );
  }

  /**
   * Save the field data associated with a layout paragraph.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request service.
   * @param \Drupal\layout_paragraphs\LayoutParagraphsLayout $layout_paragraphs_layout
   *   The layout paragraphs layout instance.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function saveInlineEditorLayoutData(
    Request $request,
    LayoutParagraphsLayout $layout_paragraphs_layout
  ): Response {
    if ($editor = $request->request->get('editor_plugin_id')) {
      $instance = $this
        ->mercuryEditorInlineEditorPluginManager
        ->createInstance($editor);

      if ($instance instanceof MercuryEditorInlineEditorInterface) {
        $uuid = $request->request->get('uuid');
        $content = $request->request->get('content');
        $field_name = $request->request->get('field_name');

        if ($component = $layout_paragraphs_layout->getComponentByUuid($uuid)) {
          $paragraph = $component->getEntity();
          $instance->saveContent(
            $content,
            $request,
            $field_name,
            $paragraph,
          );
          $paragraph->setNeedsSave(TRUE);
          $layout_paragraphs_layout->setComponent($paragraph);
          $this->layoutParagraphsLayoutTempstore->set($layout_paragraphs_layout);
        }
      }
    }

    return new AjaxResponse();
  }

  /**
   * Save the field data associated with a host entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request instance.
   * @param \Drupal\Core\Entity\ContentEntityInterface $mercury_editor_entity
   *   The mercury editor entity instance.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response object.
   */
  public function saveInlineEditorHostData(
    Request $request,
    ContentEntityInterface $mercury_editor_entity
  ): Response {
    $response = new AjaxResponse();
    try {
      if ($editor = $request->request->get('editor_plugin_id')) {
        $instance = $this
          ->mercuryEditorInlineEditorPluginManager
          ->createInstance($editor);

        if ($instance instanceof MercuryEditorInlineEditorInterface) {
          $content = $request->request->get('content');
          $field_name = $request->request->get('field_name');

          $instance->saveContent(
            $content,
            $request,
            $field_name,
            $mercury_editor_entity,
          );
          $this->mercuryEditorTempstore->set($mercury_editor_entity);
        }
      }
    }
    catch (\Exception $exception) {
      Error::logException(
        \Drupal::logger('mercury_editor_live_edit'),
        $exception
      );
    }

    return $response;
  }

  /**
   * Get the host untransformed text for a given field.
   *
   * @param string $field_name
   *   The field name.
   * @param \Drupal\Core\Entity\ContentEntityInterface $mercury_editor_entity
   *   The mercury editor entity.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A Response object.
   */
  public function getHostUntransformedText(
    string $field_name,
    ContentEntityInterface $mercury_editor_entity
  ): Response {
    $response = new AjaxResponse();

    $field = $mercury_editor_entity->get($field_name);

    $editable_text = check_markup($field->value, $field->format, 'en', [
      FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
      FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
    ]);
    $response->addCommand(new GetUntransformedTextCommand($editable_text));

    return $response;
  }

  /**
   * Get the host rendered markup for a given field.
   *
   * @param string $field_name
   *   The field name.
   * @param \Drupal\Core\Entity\ContentEntityInterface $mercury_editor_entity
   *   The mercury editor entity.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A Response object.
   */
  public function getHostRenderedField(
    string $field_name,
    ContentEntityInterface $mercury_editor_entity
  ): Response {
    $response = new AjaxResponse();

    $field = $mercury_editor_entity->$field_name->view('full');
    $response->addCommand(new GetRenderedFieldCommand($field));

    return $response;
  }

  /**
   * Get the host untransformed text for a given field.
   *
   * @param string $uuid
   *   The entity UUID.
   * @param string $field_name
   *   The field name.
   * @param \Drupal\layout_paragraphs\LayoutParagraphsLayout $layout_paragraphs_layout
   *   The layout paragraph entity instance.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   */
  public function getLayoutUntransformedText(
    string $uuid,
    string $field_name,
    LayoutParagraphsLayout $layout_paragraphs_layout
  ): Response {
    $response = new AjaxResponse();

    if ($component = $layout_paragraphs_layout->getComponentByUuid($uuid)) {
      $paragraph = $component->getEntity();

      $field = $paragraph->get($field_name);

      $editable_text = check_markup($field->value, $field->format, 'en', [
        FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
        FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
      ]);
      $response->addCommand(new GetUntransformedTextCommand($editable_text));
    }

    return $response;
  }

  /**
   * Get the layout rendered markup for a given field.
   *
   * @param string $uuid
   *   The entity UUID.
   * @param string $field_name
   *   The field name.
   * @param \Drupal\layout_paragraphs\LayoutParagraphsLayout $layout_paragraphs_layout
   *   The layout paragraph entity instance.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   */
  public function getLayoutRenderedField(
    string $uuid,
    string $field_name,
    LayoutParagraphsLayout $layout_paragraphs_layout
  ): Response {
    $response = new AjaxResponse();

    if ($component = $layout_paragraphs_layout->getComponentByUuid($uuid)) {
      $paragraph = $component->getEntity();
      $rendered_field = $paragraph->$field_name->view('_default');
      $response->addCommand(new GetRenderedFieldCommand($rendered_field));
    }

    return $response;
  }

}
