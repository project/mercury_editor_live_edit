<?php

declare(strict_types=1);

namespace Drupal\mercury_editor_live_edit;

use Drupal\node\NodeInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\mercury_editor\MercuryEditorContextService;
use Drupal\mercury_editor_live_edit\Contract\MercuryEditorInlineEditorInterface;

/**
 * Define the Mercury editor live edit module handler.
 */
class MercuryEditorLiveEditModuleHandler {

  /**
   * Define the class constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user account service.
   * @param \Drupal\mercury_editor_live_edit\MercuryEditorInlineEditorPluginManager $editorManager
   *   The Mercury editor live edit editor plugin manager.
   * @param \Drupal\mercury_editor\MercuryEditorContextService $contextService
   *   The Mercury editor context service.
   */
  public function __construct(
    protected AccountProxyInterface $account,
    protected MercuryEditorInlineEditorPluginManager $editorManager,
    protected MercuryEditorContextService $contextService
  ) {
  }

  /**
   * Attach the Mercury Inline Editor field attributes.
   *
   * @param array $variables
   *   The field variables.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function attachFieldAttributes(array &$variables): void {
    if (!$this->account->hasPermission('use mercury editor live edit')) {
      return;
    }
    $element = $variables['element'];

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    if ($entity = $element['#object']) {
      $field_definition = $entity->getFieldDefinition($element['#field_name']);

      if (
        !$entity->access('update')
        || $field_definition->isComputed()
        || !$this->isMercuryEditorActive($entity)
      ) {
        return;
      }
      $field_type = $field_definition->getType();

      $variables['attributes']['class'][] = 'me-is-editable';
      $variables['attributes']['tabindex'] = '0';

      foreach ($this->editorManager->getDefinitions() as $plugin_id => $definition) {
        if (!isset($definition['editor'])) {
          continue;
        }
        $instance = $this->editorManager->createInstance($plugin_id);

        if (
          ($instance instanceof MercuryEditorInlineEditorInterface)
          && in_array($field_type, $instance->getFieldTypes(), TRUE)
        ) {
          $element = &$variables['element'];
          $attributes = &$variables['attributes'];

          $attributes['data-me-editor_field_preview_sync_id'] = $this
            ->generateFieldSyncId($field_definition);

          $this->setMercuryEditorAttributes(
            $element,
            $attributes,
            $variables,
            $entity,
            $field_definition,
            $instance
          );
        }
      }
      $variables['#attached']['library'][] = 'mercury_editor_live_edit/inline_editor.preview.sync';
    }
  }

  /**
   * Attach the Mercury Inline Editor form attributes.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function attachFormAttributes(
    array &$form,
    FormStateInterface $form_state
  ): void {
    if (!$this->account->hasPermission('use mercury editor live edit')) {
      return;
    }

    /** @var \Drupal\mercury_editor\Entity\MercuryEditorNodeForm $form_object */
    $form_object = $form_state->getFormObject();
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $form_object->getEntity();
    $field_definitions = $entity->getFieldDefinitions();

    if ($title_definition = $field_definitions['title']) {
      $form['title']['#attributes']['data-me-editor_field_parent_sync_id'] = $this
        ->generateFieldSyncId($title_definition);
    }

    foreach ($field_definitions as $field_name => $field_definition) {
      if (
        !$field_definition instanceof FieldConfig
        || is_subclass_of(
          $field_definition->getItemDefinition()->getClass(),
          EntityReferenceItem::class
        )
      ) {
        continue;
      }
      $field_type = $field_definition->getType();

      foreach ($this->editorManager->getDefinitions() as $plugin_id => $definition) {
        if (!isset($definition['editor'])) {
          continue;
        }
        $instance = $this->editorManager->createInstance($plugin_id);

        if (
          ($instance instanceof MercuryEditorInlineEditorInterface)
          && in_array($field_type, $instance->getFieldTypes(), TRUE)
        ) {
          $element = &$form[$field_name];
          $attributes = &$element['#attributes'];

          $attributes['data-me-editor_field_parent_sync_id'] = $this
            ->generateFieldSyncId($field_definition);

          $this->setMercuryEditorAttributes(
            $element,
            $attributes,
            $form,
            $entity,
            $field_definition,
            $instance
          );
        }
      }
    }

    $form['#attached']['library'][] = 'mercury_editor_live_edit/inline_editor.parent.sync';
  }

  /**
   * Set the Mercury Editor attributes.
   *
   * @param array $element
   *   The field element.
   * @param array $attributes
   *   The attributes array.
   * @param array $container
   *   The container on which attachments should be associated.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The field entity instance.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition instance.
   * @param \Drupal\mercury_editor_live_edit\Contract\MercuryEditorInlineEditorInterface $instance
   *   The mercury inline editor plugin instance.
   */
  protected function setMercuryEditorAttributes(
    array &$element,
    array &$attributes,
    array &$container,
    ContentEntityInterface $entity,
    FieldDefinitionInterface $field_definition,
    MercuryEditorInlineEditorInterface $instance,
  ):void {
    $attributes['data-me-editor_id'] = $instance->getEditor();
    $attributes['data-me-editor_plugin_id'] = $instance->getPluginId();

    $attributes['data-me-endpoint'] = $this->resolveSaveEndpoint($entity);
    $attributes['data-me-field_name'] = $field_definition->getName();
    $attributes['data-me-entity_uuid'] = $entity->uuid();

    $attributes = array_merge(
      $attributes,
      $instance->attachAttributes($element, $entity, $field_definition)
    );

    $container['#attached'] = $instance->getAttachments($element);
  }

  /**
   * Generate a sync id for a provide field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition instance.
   *
   * @return string
   *   The field sync ID.
   */
  protected function generateFieldSyncId(
    FieldDefinitionInterface $field_definition
  ): string {
    return sha1(implode(':', [
      $field_definition->getName(),
      $field_definition->getType()
    ]));
  }

  /**
   * Resolve the Mercury editor field endpoint.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to resolve the endpoint for.
   *
   * @return string|null
   *   The Mercury editor endpoint, otherwise null if the entity type is
   *   unsupported.
   */
  protected function resolveSaveEndpoint(
    ContentEntityInterface $entity,
  ): ?string {
    if ($entity instanceof ParagraphInterface) {
      /** @var LayoutParagraphsLayout $layout_paragraph_layout */
      $layout_paragraph_layout = $entity->_layoutParagraphsLayout;
      $layout_id = $layout_paragraph_layout->id();
      return "/mercury-editor-inline-editor/layout/$layout_id/save";
    }

    if ($entity instanceof NodeInterface) {
      return "/mercury-editor-inline-editor/host/{$entity->uuid()}/save";
    }

    return NULL;
  }

  /**
   * Determine if the mercury editor is active.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to check.
   */
  protected function isMercuryEditorActive(ContentEntityInterface $entity): bool {

    $route_match = \Drupal::routeMatch();
    $route_name = $route_match->getRouteName();

    if ($route_match->getParameter('mercury_editor_entity')) {
      $me_entity = \Drupal::routeMatch()->getParameter('mercury_editor_entity');
    }
    elseif (str_ends_with($route_name, '.mercury_editor_preview')) {
      $entity_type = explode('.', $route_name)[1];
      $me_entity = $route_match->getParameter($entity_type);
    }
    elseif (
      str_starts_with($route_name, 'mercury_editor.') &&
      $route_match->getParameter('layout_paragraphs_layout')) {
      $me_entity = $route_match->getParameter('layout_paragraphs_layout')->getEntity();
    }
    else {
      return FALSE;
    }

    if ($me_entity->uuid() === $entity->uuid()) {
      return TRUE;
    }
    if (
      $entity instanceof ParagraphInterface &&
      !empty($entity->_layoutParagraphsLayout) &&
      $entity->_layoutParagraphsLayout->getEntity()->uuid() === $me_entity->uuid()) {
      return TRUE;
    }
    return FALSE;
  }

}
