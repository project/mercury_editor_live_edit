<?php

declare(strict_types=1);

namespace Drupal\mercury_editor_live_edit;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\mercury_editor_live_edit\Annotation\MercuryEditorInlineEditor;
use Drupal\mercury_editor_live_edit\Contract\MercuryEditorInlineEditorInterface;

/**
 * Define the mercury editor inline editor plugin manager.
 */
final class MercuryEditorInlineEditorPluginManager extends DefaultPluginManager {

  /**
   * The class constructor.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/MercuryEditorInlineEditor',
      $namespaces,
      $module_handler,
      MercuryEditorInlineEditorInterface::class,
      MercuryEditorInlineEditor::class
    );
    $this->alterInfo('mercury_editor_inline_editor_info');
    $this->setCacheBackend(
      $cache_backend,
      'mercury_editor_inline_editor_plugins'
    );
  }

}
