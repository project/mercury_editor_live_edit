<?php

declare(strict_types=1);

namespace Drupal\mercury_editor_live_edit\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Define the mercury editor inline editor annotation.
 *
 * @Annotation
 */
final class MercuryEditorInlineEditor extends Plugin {

  /**
   * The inline editor plugin ID.
   */
  public readonly string $id;

  /**
   * The inline editor plugin label.
   */
  public readonly string $label;

  /**
   * The inline editor plugin description.
   */
  public readonly string $description;

  /**
   * The inline editor plugin supported field types.
   */
  public readonly array $field_types;

}
