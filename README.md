# Mercury Editor Live Edit

The Mercury Editor Live Edit module provides a method for two-way syncing between the Drupal sidebar and the Mercury Editor preview edit screen. The module also allows for updating either Textarea or plain text fields without needing to open the edit screen for that component; all text edits will occur inline.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/mercury_editor_live_edit).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/mercury_editor_live_edit).

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following modules:

- [Mercury Editor](https://www.drupal.org/project/mercury_editor)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will attach to all content types that the Mercury Editor is
configured for. After enabling the module, you should be able to hover over any
plain text or textarea and be presented with an inline editor after clicking on
the content you wish to edit.

## Maintainers

- Travis Tomka - [droath](https://www.drupal.org/u/droath)
- Justin Toupin - [justin2pin](https://www.drupal.org/u/justin2pin)
